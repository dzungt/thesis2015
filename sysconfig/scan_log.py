import os, subprocess, pymongo
from pymongo import MongoClient
# Select username logged in with ip 10.10.10.106
# for log in ap.find({"ip":"10.10.10.106", "action": "login", "time":{"$lt":"1431185236"}}).sort("time", pymongo.DESCENDING).limit(1):
mongo = MongoClient()
ap = mongo.ap.access

def main():
	ip = {} # log action of a ip in time order
	command = 'cat /var/log/squid/access.log.0 | grep "DENIED"'
	try:
		result = run_command(command)
	except:
		return
	if result:
		logs = result.split('\n')
		for log in logs:
			log_parts = log.split()
			if log_parts:
				client_ip = log_parts[2] 
				if client_ip not in ip:
					ip[client_ip] = [log_parts]
				else:
					ip[client_ip].append(log_parts)

	global ap 
	
	for client_ip in ip:
		logs = ip[client_ip]
		for log in logs:
			action_time = int(float(log[0]))
			print action_time
			login_log = get_login_time_from_ip(client_ip, action_time)
			if login_log:
				username = login_log["username"]
				print username
				print str(login_log["time"]) + " : " + str(action_time)
				denied_url = log[6]
				data = {"username": username, "ip":client_ip, "time":action_time, "action": "denied", "url": denied_url}
				try:
					ap.insert_one(data)
				except:
					continue

def get_login_time_from_ip(client_ip, action_time):
	global ap
	result = ''
	for log in ap.find({"ip": client_ip, "action": "login", \
		"time":{"$lt": action_time}}).sort("time", pymongo.DESCENDING).limit(1):
		result = log
	return result

def run_command(command):
    result = subprocess.check_output(command, shell=True)
    return result

if __name__ == '__main__':
	main()
