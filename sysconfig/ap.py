import redis, threading, time, os, pymongo
from pymongo import MongoClient
loggedin_ips = {}

# configure 
server_ip = '10.10.10.140'
shell_on_logout = '/var/www/html/sysconfig/shell_on_logout.sh'

# connect to mongodb server
mongo = MongoClient()
ap = mongo.ap.access

class ThreadIPtables(threading.Thread):
	def __init__(self, threadID):
		threading.Thread.__init__(self)
		self.threadID = threadID
		

	def run(self):
		global loggedin_ips
		global server_ip
	 	r = redis.StrictRedis()
		p = r.pubsub(ignore_subscribe_messages=True)
		p.subscribe('ap')
		while True:
			message = p.get_message()
			if message:
				data = message["data"]
	
				messages = data.split('||')

				message_type = messages[0]
				client_username = messages[1]
				client_ip = messages[2]
				# print message_type + ':' + client_ip + ':' + client_username
				if message_type == 'login':
					if client_ip in loggedin_ips or client_ip == server_ip:
						continue
					else:
						print "user: " + client_username + " logged in on IP: " + client_ip
						log_to_mongo_db(client_username,client_ip,"login")
						loggedin_ips[client_ip] = client_username
						ip_notification()
				elif message_type == 'logout':
					if client_ip in loggedin_ips:
						print "user: " + client_username + " logged out on IP: " + client_ip
						log_to_mongo_db(client_username,client_ip,"logout")
						del loggedin_ips[client_ip]
						ip_notification()

				commands = messages[3:]
				for command in commands:
					print command
					os.system(command)
			time.sleep(0.001)

class ThreadIPLockoutCheck(threading.Thread):
	def __init__(self, threadID):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.threadlist = []
	def run(self):
		global loggedin_ips
		while True:
			self.threadlist = []
			for ip in loggedin_ips:
				th_checkip = ThreadPingIP(ip)
				self.threadlist.append(th_checkip)
			for th in self.threadlist:
				th.start()
			for th in self.threadlist:
				th.join()
			time.sleep(30)

class ThreadPingIP(threading.Thread):
	def __init__(self, threadID):
		threading.Thread.__init__(self)
		self.threadID = str(threadID)
		self.ip = str(threadID)
	def run(self):
		global loggedin_ips
		global server_ip
		global shell_on_logout
		ip = self.ip
		r = os.system("ping -c 1 -q -w 1 -W 0.5 %s" % ip)
		# logged out
		if r:
			if ip in loggedin_ips:			
				print "user: " + loggedin_ips[ip] + " logged out on IP: " + ip
				# log to mongodb
				username = loggedin_ips[ip]
				log_to_mongo_db(username,ip,"logout")
				# delete ip in dictionary
				del loggedin_ips[ip]
				ip_notification()

				# execute command for iptables
				shell_file = open(shell_on_logout, 'r')
				for line in shell_file:
					command  = line.replace("$CIP", ip)
					print command
					os.system(command)
				

def log_to_mongo_db(username,ip,action):
	global ap
	action_time = int(time.time())
	data = {"username": username, "ip":ip, "time":action_time, "action": action}
	try:
		ap.insert_one(data)
	except:
		pass

def ip_notification():
	global loggedin_ips
	print "IP array: "
	for ip in loggedin_ips:
		print ip + ":" + loggedin_ips[ip] + " || "

def main():
	threadIPtables = ThreadIPtables('iptables')
	threadIPtables.start()
	threadIPlogoutcheck = ThreadIPLockoutCheck('iplockoutcheck')
	threadIPlogoutcheck.start()
if __name__ == '__main__':
	main()