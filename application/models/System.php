<?php
class Model_System extends Zend_Db_Table{
	protected $_name = 'system';
  	protected $_primary = 'id';
  	
  	public function getAll(){
  		$result = $this->fetchAll()->toArray();
  		return $result;
  	}
  	public function updateConfig($data){
        $where = ' file  ="'.$data["file"].'"';
        $row = $this->fetchRow($where);
        $row->location = $data["location"];
        $row->save();

        return 0;
  	}

    public function getAttr($col, $value, $attr){
        $where = $col.' = '.'"'.$value.'"';
        $result = $this->fetchRow($where)->toArray();
        if($result)
            return $result[$attr];
        return null;
    }

    public function saveFile($location, $content){
        $file = fopen($location,"w");
        fwrite($file,$content);
        fclose($file);
    }
}