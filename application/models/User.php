<?php
/**
 * Created by PhpStorm.
 * User: dungdc40
 * Date: 4/12/15
 * Time: 11:16 PM
 */
Class Model_User extends Zend_Db_Table{
    protected $_name = 'user';
    protected $_primary = 'id';

    public function countUser($arrParam = null, $role = 0){

        $select = $this->select()->where('role = ?', $role)
            ->from('user',array('id'));
        //echo $select;

        $result = $this->fetchAll($select);
        return count($result);

    }

    public function addUser($data){
        $data = array(
            'username' => $data["user_name"],
            'pass' => $data["pass"],
            'salt' => $data["salt"],
            'role' => $data["role"],
            'email' => $data["email"],
            'id_num' => $data["id_num"],
            'first_name' => $data["first_name"],
            'last_name' => $data["last_name"],
            'created_date' => $data["created_date"]
        );
        $row = $this->createRow($data);
        $row->save();
    }

    public function getAllUser($arrParam = null, $role=0){

        $select = $this->select()->where('role = ?', $role);
        $order = ' created_date ASC';

        $paginator = $arrParam['paginator'];
        if($paginator['itemCountPerPage'] > 0){
            $page = $paginator['currentPage'];
            $rowCount = $paginator['itemCountPerPage'];
            $select->limitPage($page,$rowCount);
        }
        $result = $this->fetchAll($select,$order)->toArray();
        return $result;
    }

    public function getUser($username){
        $where = 'username="'.$username.'"';
        $result = $this->fetchRow($where)->toArray();
        return $result;
    }
    public function updateUser($data){
        $where = 'username="'.$data["user_name"].'"';
        unset($data["user_name"]);
        $this->update($data,$where);
        return 0;
    }
    public function updatePass($data){
        $where = 'username="'.$data["user_name"].'"';
        $raw_pass = $data["pass"];
        $result = $this->fetchRow($where)->toArray();
        $salt = $result['salt'];
        $new_pass = md5($raw_pass.$salt);
        $data = array("pass" => $new_pass);
        $this->update($data,$where);
        return 0;
    }
    public function delUser($username){
        $where = 'username="'.$username.'"';
        $result = $this->delete($where);
        return $result;
    }
}