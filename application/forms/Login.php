<?php
class Form_Login extends Zend_Form
{

    public function __construct($options = null)
   {
      parent::__construct($options);
      $this->setName('login');
      $username = new Zend_Form_Element_Text('username');
      $username->setLabel('Username')
                ->setAttrib('size',25)
               ->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Empty!')))
               ->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Empty!')))
                ->setRequired(true);
                
      $password = new Zend_Form_Element_Password('password');
      $password->setLabel('Password')
                ->setAttrib('size', 25)
               ->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Empty!')))
                ->setRequired(true);
                
      $submit = new Zend_Form_Element_Submit('submit');
      $this->addElements(array($username, $password, $submit));
      
   }
}