<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
	protected function _initAutoload() {
		$autoloader = new Zend_Application_Module_Autoloader(
							array ('namespace' => '', 
								   'basePath' => APPLICATION_PATH )
							);

		return $autoloader;
	
	}

    protected  function _initSession(){
        Zend_Session::start();
//        Zend_Session::rememberMe(864000);
    }

    protected function _initDb(){

        $optionResources = $this->getOption('resources');
        $dbOption = $optionResources['db'];
        $dbOption['params']['username'] = 'root';
        $dbOption['params']['password'] = '';
        $dbOption['params']['dbname'] = 'ap';

        $adapter = $dbOption['adapter'];
        $config = $dbOption['params'];

        $db = Zend_Db::factory($adapter,$config);
        $db->setFetchMode(Zend_Db::FETCH_ASSOC);
        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");

        Zend_Registry::set('connectDb',$db);

        Zend_Db_Table::setDefaultAdapter($db);

        return $db;

    }
}