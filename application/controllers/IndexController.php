<?php
/**
 * Created by PhpStorm.
 * User: dungdc40
 * Date: 4/9/15
 * Time: 10:22 PM
 */
class IndexController extends AP_Controller_Action{

    public function init(){
        parent::init();

        $cip = $this->getRequest()->getServer('REMOTE_ADDR'); // client ip
        $sip = $_SERVER['SERVER_ADDR']; //server ip
        // redirect to real ip of server
        if($_SERVER['HTTP_HOST'] != $sip){
            $this->_redirect('http://'.$_SERVER['SERVER_ADDR']);
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
        }
//        $this->_helper->viewRenderer->setNoRender();
//        $this->_helper->layout->disableLayout();
        else{

            $template_path = TEMPLATE_PATH . "/login";
            $this->loadTemplate($template_path,'template.ini','template');
            //Duong dan cua Action chinh
            $this->_actionMain = '/' . $this->_arrParam['controller'] . '/index';

            $auth = Zend_Auth::getInstance();
            if($auth->hasIdentity()){
                $user = $auth->getIdentity();
                if($cip != $sip){ // da tung dang nhap trong qua khu
                    if($this->_currentController == 'index'){
                        //$ip = $cip//$user->ip;
                        $user->ip = $cip; // luu ip moi cua may
                        $auth->getStorage()->write($user);
                        $redis = new AP_Redis($cip, $user->username);
                        $redis->loginshell();

                        // log access
                        // $mongo = new AP_Mongo($user->ip, $user->username);
                        // $mongo->logaccess('login');
                    }
                    
                }
                if($user->role == 1)
                    $this->_helper->redirector('index', 'admin');
                else if($user->role == 0)
                    $this->_helper->redirector('index', 'user');
            }
            else if(!$auth->hasIdentity()){
                if($cip == $sip){ // vua logout --> force restart
                    echo "Please Restart Browser For Your Previous Action To Take Effect";
                    $this->_helper->viewRenderer->setNoRender();
                    $this->_helper->layout->disableLayout();
                }
            }

                  
        }

    }

    public function indexAction(){
      
        $ip = $this->getRequest()->getServer('REMOTE_ADDR');
        $this->view->ip_addr = $ip;
        //1. Goi ket noi voi Zend Db
        $db = Zend_Registry::get('connectDb');

        //2.Khoi tao Zend Auth
        $auth = Zend_Auth::getInstance();

        //3
        $authAdapter = new Zend_Auth_Adapter_DbTable($db);
        //Zend_Db_Adapter_Abstract $zendDb = null, $tableName = null, $identityColumn = null,
        //                        $credentialColumn = null, $credentialTreatment = null)
        $authAdapter->setTableName('user')
            ->setIdentityColumn('username')
            ->setCredentialColumn('pass');

        //->where('active_code = ?','',STRING);

        if($this->_request->isPost()){
            $user_name = $this->_arrParam['username'];
            $password = $this->_arrParam['password'];
            $authAdapter->setIdentity($user_name);
            $authAdapter->setCredential($password);
            $authAdapter->setCredentialTreatment("MD5( CONCAT(?, salt))");

            //Lay ket qua truy van cua Zend_Auth
            $result = $auth->authenticate($authAdapter);
            if(!$result->isValid()){
                $error = $result->getMessages();
                echo '<br>' . current($error);
            }else{
                //Lay thong tin cua tai khoan dua vao session cua Zend Auth
                //$returnColumns = null, $omitColumns = null
                $omitColumns = array('pass');
                $data = $authAdapter->getResultRowObject(null,$omitColumns);
                $ip = $this->getRequest()->getServer('REMOTE_ADDR');

                $redis = new AP_Redis($ip, $user_name);
                $redis->loginshell();

                // log access
                // $mongo = new AP_Mongo($ip, $user_name);
                // $mongo->logaccess('login');

                $data->ip = $ip;

                $auth->getStorage()->write($data);
                Zend_Session::rememberMe(86400);

                if($data->role == 0){
                    
                    $this->_helper->redirector('index', 'user');
                }
                else if($data->role == 1){
                    // redirect to admin panel
                    $this->_helper->redirector('index', 'admin');
                }
            }

        }



    }

    public function registerAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        if($this->_request->isPost()){
            $user_name = $this->_arrParam['username'];
            $pass = $this->_arrParam['pass'];
            $re_pass = $this->_arrParam['re_pass'];
            $email = $this->_arrParam['email'];
            $first_name = $this->_arrParam['first_name'];
            $last_name = $this->_arrParam['last_name'];
            $id_num = $this->_arrParam['id_num'];

            $data = array(
                "user_name" =>  $user_name, "pass" => $pass, "re_pass" => $re_pass,
                "email" => $email, "first_name" => $first_name, "last_name" => $last_name,
                "id_num" => $id_num
            );

            $check_user_info = new AP_Validate_Register();
            $errors = $check_user_info->isValid($data);
            if(!$errors){
                $data["salt"] = substr(md5($data["user_name"]), 0, 20);
                $data["pass"] = md5($data["pass"].$data["salt"]);
                $data["created_date"] = date('Y-m-d', time());
                $data["role"] = 0;
                $db = new Model_User();
                $db->addUser($data);
                $this->_helper->flashMessenger->addMessage('Create Account Successfully');
                $this->_helper->redirector('index', 'admin');
            }
            else{
                foreach($errors as $type => $error)
                    $this->_helper->flashMessenger->addMessage($error);
                $this->_helper->redirector('index', 'index');

            }


        }


    }


}