<?php
/**
 * Created by PhpStorm.
 * User: dungdc40
 * Date: 5/4/15
 * Time: 8:29 PM
 */
class UserController extends AP_Controller_Action{

    public $_username = '';
    public function init(){

        parent::init();

        $auth = Zend_Auth::getInstance();
        // redirect if haven't login
        if(!$auth->hasIdentity()){
//            echo "don't have access right";
            $this->_helper->redirector('index', 'index');

        }
        $user = $auth->getIdentity();
        $role = $user->role;
        if($role == '1')
            $role = 'Admin';
        else $role = 'Guest';

        if(!$this->_acl->isAllowed($role, $this->_currentController, null)){
            $this->_helper->redirector('index', 'index');
        }
        $user = $auth->getIdentity();
        $this->_username = $user->username;
//        $this->_helper->viewRenderer->setNoRender();
        $template_path = TEMPLATE_PATH . "/user";
        $this->loadTemplate($template_path,'template.ini','template');
    }

    public function indexAction(){
        if($this->_request->isPost()){
            $user = new Model_User();
            $data = array();
            foreach($this->_arrParam as $key => $val){
                if($key != 'controller' and $key != 'action' and $key != 'module' and $key != 'paginator'){
                    $val = htmlentities($val, ENT_QUOTES);
                    $data[$key] = $val;
                }
            }
            $data['user_name'] = $this->_username;
            $check_user_info = new AP_Validate_Register($action="update");
            $errors = $check_user_info->isValid($data);
            if(!$errors){
                $user->updateUser($data);
                $this->_helper->flashMessenger->addMessage('Update Account Successfully');
            }
            else{
                foreach($errors as $type => $error)
                    $this->_helper->flashMessenger->addMessage($error);
            }
            $this->_helper->redirector('index', 'user');
        }
        $user = new Model_User();
        $user_info = $user->getUser($this->_username);
        $this->view->user = $user_info;
    }

    public function changepassAction(){
        if($this->_request->isPost()){
            $pass = $this->_arrParam["pass"];
            $repass = $this->_arrParam["repass"];
            $data = array('pass' => $pass, 're_pass' => $repass);
            $check_pass = new AP_Validate_Register($action="changepass");
            $errors = $check_pass->isValid($data);
            if(!$errors){
                $user = new Model_User();
                $data['user_name'] = $this->_username;
                print_r($data);
                $user->updatePass($data);
                $this->_helper->flashMessenger->addMessage('Update Password Successfully');
            }
            else{
                foreach($errors as $type => $error)
                    $this->_helper->flashMessenger->addMessage($error);
            }
            $this->_helper->redirector('changepass', 'user');
        }
    }
}