<?php
/**
 * Created by PhpStorm.
 * User: dungdc40
 * Date: 4/9/15
 * Time: 10:22 PM
 */
class LoginController extends AP_Controller_Action{
    public function init(){
        parent::init();
        $this->_helper->viewRenderer->setNoRender();
        $template_path = TEMPLATE_PATH . "/login";
        $this->loadTemplate($template_path,'template.ini','template');
    }
    public function indexAction(){

    }
}