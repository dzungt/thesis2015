<?php
/**
 * Created by PhpStorm.
 * User: dungdc40
 * Date: 4/9/15
 * Time: 10:22 PM
 */
class AdminController extends AP_Controller_Action{
    public $_ip = '';
    public $_username = '';
    public function init(){

        parent::init();

        $auth = Zend_Auth::getInstance();
        // redirect if haven't login
        if(!$auth->hasIdentity()){
//            echo "don't have access right";
            $this->_helper->redirector('index', 'index');
        }
        $user = $auth->getIdentity();
        $role = $user->role;
        if($role == '1')
            $role = 'Admin';
        else $role = 'Guest';

        if(!$this->_acl->isAllowed($role, $this->_currentController, null)){
            $this->_helper->redirector('index', 'index');
        }

//        $this->_helper->viewRenderer->setNoRender();
        $template_path = TEMPLATE_PATH . "/admin";
        $this->loadTemplate($template_path,'template.ini','template');

        $this->_ip = $user->ip;
        $this->_username = $user->username;
    }
    public function indexAction(){
        if($this->_request->isPost()){
            $add_config = new Model_System();
            foreach($this->_arrParam as $key => $val){
                if($key != 'controller' and $key != 'action' and $key != 'module' and $key != 'paginator'){
                    $val = htmlentities($val, ENT_QUOTES);
                    $data = array(
                        "file" => $key,
                        "location" => $val
                    );
                    $add_config->updateConfig($data);
                }
            }
        }
        $system = new Model_System();
        $new_config = $system->getAll();
        $this->view->system_conf = $new_config;
    }

    public function commandAction(){
        $system = new Model_System();
        $shell_on_login_location = $system->getAttr("file", "on_login_shell", "location");
        $shell_on_logout_location = $system->getAttr("file", "on_logout_shell", "location");
        $dhcp_on_release_location = $system->getAttr("file", "dhcp_on_release_shell", "location");
        $dhcp_on_release_content = $shell_on_login_content = $shell_on_logout_content = "";

        if($this->_request->isPost()){
            $shell_on_login_content = $this->_arrParam["on_login_shell"];
            $shell_on_logout_content = $this->_arrParam["on_logout_shell"];
            $dhcp_on_release_content = $this->_arrParam["dhcp_on_release_shell"];

            if(is_file($shell_on_login_location))
                $system->saveFile($shell_on_login_location, $shell_on_login_content);
            if(is_file($shell_on_logout_location))
                $system->saveFile($shell_on_logout_location, $shell_on_logout_content);
            if(is_file($dhcp_on_release_location))
                $system->saveFile($dhcp_on_release_location, $dhcp_on_release_content);

        }
        else{
            if(is_file($shell_on_login_location))
                $shell_on_login_content = file_get_contents($shell_on_login_location);
            if(is_file($shell_on_logout_location))
                $shell_on_logout_content = file_get_contents($shell_on_logout_location);
            if(is_file($dhcp_on_release_location))
                $dhcp_on_release_content = file_get_contents($dhcp_on_release_location);
        }
        $this->view->dhcp_on_login_content = $shell_on_login_content;
        $this->view->dhcp_on_logout_content = $shell_on_logout_content;
        $this->view->dhcp_on_release_content = $dhcp_on_release_content;
    }

    public function userAction(){
        $users_model = new Model_User();
        $members = $users_model->getAllUser($this->_arrParam, 0);

        $total_user = $users_model->countUser($role=0);
        $paginator = new AP_Paginator();
        $this->view->panigator = $paginator->createPaginator($total_user,$this->_paginator);

        $this->view->members = $members;
    }

    public function adduserAction(){
        if($this->_request->isPost()){
            $user_name = $this->_arrParam['username'];
            $pass = $this->_arrParam['pass'];
            $re_pass = $this->_arrParam['re_pass'];
            $email = $this->_arrParam['email'];
            $first_name = $this->_arrParam['first_name'];
            $last_name = $this->_arrParam['last_name'];
            $id_num = $this->_arrParam['id_num'];
            $role = $this->_arrParam['role'];
            $data = array(
                "user_name" =>  $user_name, "pass" => $pass, "re_pass" => $re_pass,
                "email" => $email, "first_name" => $first_name, "last_name" => $last_name,
                "id_num" => $id_num, "role" => $role
            );

            $check_user_info = new AP_Validate_Register();
            $errors = $check_user_info->isValid($data);
            if(!$errors){
                $data["salt"] = substr(md5($data["user_name"]), 0, 20);
                $data["pass"] = md5($data["pass"].$data["salt"]);
                $data["created_date"] = date('Y-m-d', time());
                $data["role"] = 0;
                $db = new Model_User();
                $db->addUser($data);
                $this->_helper->flashMessenger->addMessage("Create new user successfully");
                $this->_helper->redirector('user', 'admin');
            }
            else{

                foreach($errors as $type => $error){
                    $this->_helper->flashMessenger->addMessage($error);
                }
                $this->_helper->redirector('adduser', 'admin');
            }

        }
    }

    public function edituserAction(){

        if($this->_request->isPost()){
            $user_name = $this->_arrParam['user'];
            $email = $this->_arrParam['email'];
            $first_name = $this->_arrParam['first_name'];
            $last_name = $this->_arrParam['last_name'];
            $id_num = $this->_arrParam['id_num'];
            $role = $this->_arrParam['role'];
            $data = array(
                "user_name" =>  $user_name,
                "email" => $email, "first_name" => $first_name, "last_name" => $last_name,
                "id_num" => $id_num, "role" => $role
            );

            $check_user_info = new AP_Validate_Register($action="edit");
            $errors = $check_user_info->isValid($data);

            if(!$errors){
                $db = new Model_User();
                $db->updateUser($data);
                $this->_helper->flashMessenger->addMessage("Update user successfully");
                $this->_helper->redirector('user', 'admin');
            }
            else{
                foreach($errors as $type => $error){
                    $this->_helper->flashMessenger->addMessage($error);
                }
                $this->_helper->redirector('edituser', 'admin', 'default', array('user' => $user_name));
            }

        }
        $username = $this->_arrParam["user"];
        $db = new Model_User();
        $user_info = $db->getUser($username);
        $this->view->user = $user_info;
    }

    public function deluserAction(){
        $this->_helper->viewRenderer->setNoRender();
        $username = $this->_arrParam["user"];
        $db = new Model_User();
        $result = $db->delUser($username);
        if($result != 0){
            $this->_helper->flashMessenger->addMessage("Delete user successfully");
        }
        else{
            $this->_helper->flashMessenger->addMessage("Can't delete user");
        }
        $this->_helper->redirector('user', 'admin');
    }

    public function infouserAction(){
        $username = $this->_arrParam["user"];
        $db = new Model_User();
        $user_info = $db->getUser($username);
        $this->view->user = $user_info;
    }

    // content management via squid
    public function contentAction(){
        $system = new Model_System();
        $web_block_list_location = $system->getAttr("file", "web_block_list", "location");
        $web_block_list_content = '';

        if($this->_request->isPost()){
            $web_block_list_content = $this->_arrParam["web_block_list"];

            if(is_file($web_block_list_location))
                $system->saveFile($web_block_list_location, $web_block_list_content);

        }
        else{
            if(is_file($web_block_list_location))
                $web_block_list_content = file_get_contents($web_block_list_location);

        }
        $this->view->dhcp_on_login_content = $web_block_list_content;
    }

    public function getactivitybydateAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if (isset($_POST)) {

            $username = $this->_arrParam["username"];
            $datefrom = $this->_arrParam["datefrom"];
            $dateto = $this->_arrParam["dateto"];
            
            if($dateto >= $datefrom){
                $mongo = new AP_Mongo($this->_ip, $username);
                $logs = $mongo->getlog($datefrom, $dateto);

                $result = array();
                
                foreach ($logs as $id => $value) {
                    $action = '';
                    if($value["action"] == "denied")
                        // $action = array("denied" => $value["url"]);
                        $action = "Access dinied web: ".$value["url"];
                    else $action = $value["action"];
                    $time = date('Y-m-d H:i:s', $value["time"]);
                    $result[$time] = $action;
                  
                }
                
                if(empty($result))
                    echo "0";
                else 
                    echo json_encode($result);
                
            }          
            exit;
        }

    }

    // public function getactivitybydate1Action(){
    //     $this->_helper->layout()->disableLayout();
    //     $this->_helper->viewRenderer->setNoRender(true);
    //     $username = "dungdc40";
    //     $datefrom = "1430463600";
    //     $dateto = "1430550000";
    //     if($dateto >= $datefrom){
    //         $mongo = new AP_Mongo($this->_ip, $username);
    //         $logs = $mongo->getlog($datefrom, $dateto);

    //         $result = array();
            
    //         foreach ($logs as $id => $value) {
    //             $action = '';
    //             if($value["action"] == "denied")
    //                 // $action = array("denied" => $value["url"]);
    //                 $action = "Access dinied web: ".$value["url"];
    //             else $action = $value["action"];
    //             $time = date('Y-m-d H:i:s', $value["time"]);
    //             $result[$time] = $action;
              
    //         }
            
    //         if($result)
    //             echo json_encode($result);
    //         else echo "123";
            
    //     }
    //     // var_dump($this->_arrParam);
    // }

}