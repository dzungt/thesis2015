<?php
class AP_Validate_Register{

    public $action;
    public $error = array();

    public function __construct($action="create"){
        $this->action = $action;
    }
    public function isValid($data){
        if($this->action != 'changepass'){
            $this->isValidUserName($data["user_name"]);
            $this->isValidEmail($data["email"]);
        }
        if($this->action != 'update'){
            $this->comparePass($data["pass"], $data["re_pass"]);
        }
        return $this->error;
    }
    public function isValidEmail($email){
        $validator = new Zend_Validate_EmailAddress();
        if(!$validator->isValid($email)){
            $messages = $validator->getMessages();
            $this->error["email"] = current($messages);
        }
    }

    public function isValidUserName($user_name){
        if($this->action == 'create'){
            $options = array('table'=>'user', 'field'=>'username');
            $validator = new Zend_Validate_Db_NoRecordExists($options);
            if(!$validator->isValid($user_name)){
                $messages = $validator->getMessages();
                $this->error["user_name"] =  "UserName Exist!";
            }
        }
    }
    public function comparePass($pass, $re_pass){
        if(strcmp($pass, $re_pass) != 0){
            $this->error["pass"] = "Passwords Not Match!";
        }
    }
}