<?php
class AP_Mongo{
    public $cip = '';
    public $username = '';
    public $connection = '';

    public function __construct($cip, $username){
        $this->cip = $cip;
        $this->username = $username;
        $this->connection = new MongoClient();
        $this->collection = $this->connection->ap->access;
    }


    public function logaccess($type='login'){
        $action = "";
        if($type == 'login')
            $action = "login";
        else if($type == 'logout')
            $action = "logout";
        $time = (int) time();
        $log = array(
            "username" => $this->username,
            "ip" => $this->cip,
            "time" => $time,
            "action" => $action
        );
        $this->collection->insert($log);
    }

    public function getlog($from, $to){
        $from = (int)$from;
        $to = (int)$to;
        $access = array("username" => $this->username, "time" => array("\$gt" => $from, "\$lt" => $to));
        // $access = array("username" => "dungdc40");
        $result =  $this->collection->find($access)->sort(array("time" => -1));
        return $result;
    }
}