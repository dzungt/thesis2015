<?php
class AP_Controller_Action extends Zend_Controller_Action{
    protected $_acl;

    protected $_arrParam;

    //Duong dan cua Controller
    protected $_currentController;

    //Duong dan cua Action chinh
    protected $_actionMain;

    //Thong so phan trang
    protected $_paginator = array(
        'itemCountPerPage' => 5,
        'pageRange' => 3,
    );

    protected $_message;
    public function init(){
        parent::init();
//        $template_path = TEMPLATE_PATH . "/admin";
//        $this->loadTemplate($template_path,'template.ini','template');

        //Mang tham so nhan duoc o moi Action
        $this->_arrParam = $this->_request->getParams();

        //Duong dan cua Controller
        $this->_currentController = $this->_arrParam['controller'];


        // Cap quyen truy cap
        $acl  = new Zend_Acl();

        //Khai bao cac nhom (Role)
        $acl->addRole(new Zend_Acl_Role('Guest'))
            ->addRole(new Zend_Acl_Role('Admin'));

        $acl->addResource(new Zend_Acl_Resource('admin'))
            ->addResource(new Zend_Acl_Resource('index'))
            ->addResource(new Zend_Acl_Resource('user'));

        //Cap quyen truy cap cho cac nhom BlogController
        $acl->allow('Admin',null, null);
        $acl->allow('Guest','user', null);
        $this->_acl = $acl;

        $message = $this->_helper->flashMessenger->getMessages();
        if(count($message) ==1 and $message[0] == 'redirect'){
            $this->_message = 'redirect';
        }
        else            
            if($message) $this->view->message = $message;

        $this->view->arrParam = $this->_arrParam;
        $this->view->currentController = $this->_currentController;

        $this->_paginator['currentPage'] = $this->_request->getParam('page',1);
        $this->_arrParam['paginator'] = $this->_paginator;

    }
    protected function loadTemplate($template_path, $fileConfig = 'template.ini',$sectionConfig = 'template'){

        //Xoa nhung du cua layout truoc
        $this->view->headTitle()->set('');
        $this->view->headMeta()->getContainer()->exchangeArray(array());
        $this->view->headLink()->getContainer()->exchangeArray(array());
        $this->view->headScript()->getContainer()->exchangeArray(array());

        $filename = $template_path . "/" . $fileConfig;
        $section = $sectionConfig;
        $config = new Zend_Config_Ini($filename,$section);
        $config = $config->toArray();
        $baseUrl = $this->_request->getBaseUrl();
        //$baseUrl = '/zf';

        // template file
        $templateUrl = $baseUrl .$config['url'];
        $cssUrl = $templateUrl . $config['dirCss'];
        $jsUrl = $templateUrl . $config['dirJs'];
        $imgUrl = $templateUrl . $config['dirImg'];

        // global file
        $g_cssUrl = $baseUrl.'/css';
        $g_jsUrl = $baseUrl.'/js';
        $g_imgUrl = $baseUrl.'/images';



        //Nap title cho layout
        $this->view->headTitle($config['title']);

        //Nap cac the meta vao layout
        if(count($config['metaHttp'])>0){
            foreach ($config['metaHttp'] as $key => $value){
                $tmp = explode("|",$value);
                $this->view->headMeta()->appendHttpEquiv($tmp[0],$tmp[1]);
            }
        }

        if(count($config['metaName'])>0){
            foreach ($config['metaName'] as $key => $value){
                $tmp = explode("|",$value);
                $this->view->headMeta()->appendName($tmp[0],$tmp[1]);
            }
        }

        //Nap cac tap tin CSS vao layout
        if(isset($config['fileCss'])){
            if(count($config['fileCss'])>0){
                foreach ($config['fileCss'] as $key => $css){
                    $this->view->headLink()->appendStylesheet($cssUrl . $css,'screen');
                }
            }
        }
        if(isset($config['fileGlobalCss'])){
            if(count($config['fileGlobalCss'])>0){
                foreach ($config['fileGlobalCss'] as $key => $css){
                    $this->view->headLink()->appendStylesheet($g_cssUrl . $css,'screen');
                }
            }
        }

        //Nap cac tap tin javascript cho layout
        if(isset($config['fileJs'])){
            if(count($config['fileJs'])>0){
                foreach ($config['fileJs'] as $key => $js){
                    $this->view->headScript()->appendFile($jsUrl . $js,'text/javascript');
                }
            }
        }
        if(isset($config['fileGlobalJs'])){
            if(count($config['fileGlobalJs'])>0){
                foreach ($config['fileGlobalJs'] as $key => $js){
                    $this->view->headScript()->appendFile($g_jsUrl . $js,'text/javascript');
                }
            }
        }


        $this->view->templateUrl = $templateUrl;
        $this->view->cssUrl = $cssUrl;
        $this->view->jsUrl = $jsUrl;
        $this->view->imgUrl = $imgUrl;
        $option = array('layoutPath'=> $template_path, 'layout'=> $config['layout']);
        Zend_Layout::startMvc($option);

    }
    public function logoutAction(){
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $user = $auth->getIdentity();
            $redis = new AP_Redis($user->ip, $user->username);
            $redis->logoutshell();

            // log access
            // $mongo = new AP_Mongo($ip, $user->username);
            // $mongo->logaccess('logout');
        }
        $auth->clearIdentity();
        $this->_helper->flashMessenger->addMessage('redirect');
        $this->_helper->redirector('index', 'index');
        // $this->_helper->viewRenderer->setNoRender();

        // insert to iptables
    }
}