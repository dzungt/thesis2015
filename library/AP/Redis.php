<?php
class AP_Redis{
	public $cip = '';

	public function __construct($cip, $username){
		$this->cip = $cip;
        $this->username = $username;
	}

    public function loginshell(){
        // Thuc thi lenh shell
        
        $system = new Model_System();
        $shell_on_login_location = $system->getAttr('file', 'on_login_shell', 'location');
        if($shell_on_login_location && is_file($shell_on_login_location)){
            $shell_on_login_content = trim(file_get_contents($shell_on_login_location));
            $command = explode(PHP_EOL, $shell_on_login_content);
            $command = implode('||', $command);
            $command = "login||".$this->username."||".$this->cip."||".str_replace("\$CIP", $this->cip, $command);
            $redis = new Redis();
            $redis->connect('127.0.0.1');
            $redis->publish('ap', $command); // send message.
            $redis->close();
        }
    }

   	public function logoutshell(){
   		 // Thuc thi lenh shell
        $system = new Model_System();
        $shell_on_login_location = $system->getAttr('file', 'on_logout_shell', 'location');
        if($shell_on_login_location && is_file($shell_on_login_location)){
            $shell_on_login_content = trim(file_get_contents($shell_on_login_location));
            $command = explode(PHP_EOL, $shell_on_login_content);
            $command = implode('||', $command);
            $command = "logout||".$this->username."||".$this->cip."||".str_replace("\$CIP", $this->cip, $command);
            $redis = new Redis();
            $redis->connect('127.0.0.1');
            $redis->publish('ap', $command); // send message.
            $redis->close();
        }
   	}

    // public function logaccess($type='login', $username){
    //     $redis = new Redis();
    //     $access_key = "user:".$username.":access";
    //     $ip_key = "ip:".$this->cip;
    //     $current_time = time();
    //     if($type == 'login'){
    //         $redis->lPush($access_key, $current_time.":");
    //         $redis->lPush($ip_key, $current_time.":");
    //     }
    //     else if($type == 'logout'){
    //         $access_log = $redis->lPop($access_key);
    //         $ip_log = $redis->lPop($ip_key);
    //         if(strpos(":", $access_log) != FALSE){
    //             $access_log .= $current_time;
    //         }
    //         if(strpos(":", $ip_log) != FALSE){
    //             $ip_log .= $current_time.":".$username;
    //         }
    //         $redis->lPush($access_key, $access_log);
    //         $redis->lPush($ip_key, $ip_log);
    //     }
    //     $redis->close();
    // }


}